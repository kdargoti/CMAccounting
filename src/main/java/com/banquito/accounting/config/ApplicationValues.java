package com.banquito.accounting.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class ApplicationValues {
  private final String mongoHost;
  private final String mongoDB;
  private final String mongoUsr;
  private final String mongoPwd;
  private final String mongoAut;

  public ApplicationValues(
      @Value("${banquito.accounting.mongo.host}") String mongoHost,
      @Value("${banquito.accounting.mongo.db}") String mongoDB,
      @Value("${banquito.accounting.mongo.usr}") String mongoUsr,
      @Value("${banquito.accounting.mongo.pwd}") String mongoPwd,
      @Value("${banquito.accounting.mongo.aut}") String mongoAut) {

    this.mongoHost = mongoHost;
    this.mongoDB = mongoDB;
    this.mongoUsr = mongoUsr;
    this.mongoPwd = mongoPwd;
    this.mongoAut = mongoAut;
  }
}
