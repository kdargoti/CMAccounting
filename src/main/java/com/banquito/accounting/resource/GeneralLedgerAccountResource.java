package com.banquito.accounting.resource;

import com.banquito.accounting.dto.GeneralLedgerAccountDTO;
import com.banquito.accounting.model.GeneralLedgerAccount;
import com.banquito.accounting.service.GeneralLedgerAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/GeneralLedgerAccount")
@RequiredArgsConstructor
public class GeneralLedgerAccountResource {
    private final GeneralLedgerAccountService ledgerAccountService;

    
    @GetMapping(path = "ledgerAccount/code/{code}")
    public ResponseEntity<GeneralLedgerAccount> getLedgerAccountByCode(@RequestParam String code){
        return ResponseEntity.ok(this.ledgerAccountService.searchByCode(code));
    }

    @GetMapping(path = "ledgerAccount/type/{type}")
    public ResponseEntity<GeneralLedgerAccount> getLedgerAccountByType(@RequestParam String type){
        return ResponseEntity.ok(this.ledgerAccountService.searchByType(type));
    }

    @PostMapping
    public ResponseEntity<GeneralLedgerAccountDTO> create(@RequestBody GeneralLedgerAccountDTO dto){
        try {
            GeneralLedgerAccount ledgerAccount = this.ledgerAccountService.create(this.buildLedgerAccount(dto));
            return  ResponseEntity.ok(this.buildLedgerAccountDTO(ledgerAccount));
        } catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping
    public ResponseEntity<GeneralLedgerAccountDTO> update(@RequestBody GeneralLedgerAccountDTO dto){
        try{
            GeneralLedgerAccount ledgerAccount = this.ledgerAccountService.update(
                    this.buildLedgerAccount(dto)
            );
            return ResponseEntity.ok(this.buildLedgerAccountDTO(ledgerAccount));
        }catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    private GeneralLedgerAccount buildLedgerAccount(GeneralLedgerAccountDTO dto){
        GeneralLedgerAccount ledgerAccount =
                GeneralLedgerAccount.builder()
                        .name(dto.getName())
                        .code(dto.getCode())
                        .type(dto.getType())
                        .currentBalance(dto.getCurrentBalance())
                        .openingBalance(dto.getOpeningBalance())
                        .build();
        return ledgerAccount;
    }

    private GeneralLedgerAccountDTO buildLedgerAccountDTO(GeneralLedgerAccount ledgerAccount){
        GeneralLedgerAccountDTO dto = GeneralLedgerAccountDTO.builder()
                .name(ledgerAccount.getName())
                .code(ledgerAccount.getCode())
                .type(ledgerAccount.getType())
                .currentBalance(ledgerAccount.getCurrentBalance())
                .openingBalance(ledgerAccount.getOpeningBalance())
                .build();
        return dto;
    }

}
