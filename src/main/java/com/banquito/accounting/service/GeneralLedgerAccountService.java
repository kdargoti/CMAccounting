package com.banquito.accounting.service;

import com.banquito.accounting.dao.GeneralLedgerAccountRepository;
import com.banquito.accounting.exceptions.EntityNotFoundException;
import com.banquito.accounting.model.GeneralLedgerAccount;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GeneralLedgerAccountService {

    private final GeneralLedgerAccountRepository generalLedgerAccountRepository;

    public GeneralLedgerAccount searchById(String id) {

        Optional<GeneralLedgerAccount> genelalLedgerAccountOpt = this.generalLedgerAccountRepository.findById(id);
        if (genelalLedgerAccountOpt.isEmpty()) {
            return null;
        }
        return genelalLedgerAccountOpt.get();
    }

    public List<GeneralLedgerAccount> listGeneralLedgerAccounts(){
        return this.generalLedgerAccountRepository.findAll();
    }

    public GeneralLedgerAccount searchByCode(String code) {
        Optional<GeneralLedgerAccount> genelalLedgerAccountOpt = this.generalLedgerAccountRepository.findByCode(code);
        if (genelalLedgerAccountOpt.isEmpty()) {
            return null;
        }
        return genelalLedgerAccountOpt.get();

    }

    public GeneralLedgerAccount searchByType(String type){
        Optional<GeneralLedgerAccount> genelalLedgerAccountOpt = this.generalLedgerAccountRepository.findByType(type);
        if (genelalLedgerAccountOpt.isEmpty()) {
            return null;
        }
        return genelalLedgerAccountOpt.get();
    }
    

    public GeneralLedgerAccount create(GeneralLedgerAccount ledgerAccount) {
        Optional<GeneralLedgerAccount> generalLedgerAccountDB = this.generalLedgerAccountRepository.findByCode(ledgerAccount.getCode());
        if (generalLedgerAccountDB.isPresent()) {
            throw new EntityNotFoundException(
                    "There's already a ledger Account with this code");
        }
        return this.generalLedgerAccountRepository.save(ledgerAccount);
    }

    public GeneralLedgerAccount update(GeneralLedgerAccount ledgerAccount) {
        Optional<GeneralLedgerAccount> generalLedgerAccountOpt = this.generalLedgerAccountRepository.findByCode(ledgerAccount.getCode());
        if (generalLedgerAccountOpt.isEmpty()) {
            throw new EntityNotFoundException(
                    "Ledger Account not found with this code");
        }
        GeneralLedgerAccount generalLedgerAccountDB = generalLedgerAccountOpt.get();
        generalLedgerAccountDB.setName(ledgerAccount.getName());
        generalLedgerAccountDB.setType(generalLedgerAccountDB.getType());
        generalLedgerAccountDB.setCurrentBalance(ledgerAccount.getCurrentBalance());
        return this.generalLedgerAccountRepository.save(generalLedgerAccountDB);
    }


}
