package com.banquito.accounting.service;

import com.banquito.accounting.dao.GeneralLedgerAccountRepository;
import com.banquito.accounting.dao.JournalEntryRepository;
import com.banquito.accounting.enums.LedgerAccountTypeEnum;
import com.banquito.accounting.exceptions.EntityNotFoundException;
import com.banquito.accounting.model.JournalEntry;
import com.banquito.accounting.model.JournalEntryDetail;
import com.banquito.accounting.model.GeneralLedgerAccount;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class JournalEntryService {

    private final JournalEntryRepository journalEntryRepository;
    private final GeneralLedgerAccountRepository generalLedgerAccountRepository;
    private final GeneralLedgerAccountService ledgerAccountService;

    public List<JournalEntry> getJournalEntries() {
        return this.journalEntryRepository.findAll();
    }
    
    public JournalEntry searchByTransactionReference(String transactionReference) {

        Optional<JournalEntry> journalEntryOpt = this.journalEntryRepository.findByTransactionReference(transactionReference);
        if (journalEntryOpt.isEmpty()) {
            return null;
        }
        return journalEntryOpt.get();
    }

    public List<JournalEntry> getByAccountingDate(Date dateFrom, Date dateTo) {

        return this.journalEntryRepository.findByAccountingDateBetween(dateFrom, dateTo);
    }

    public List<JournalEntry> getByLedgerAccountAndAccountingDate(String ledgerAccount, Date dateFrom, Date dateTo) {

        return this.journalEntryRepository.findByJournalEntriesLedgerAccountAndAccountingDateBetween(ledgerAccount,dateFrom, dateTo);
    }

    public List<JournalEntry> getByGeneralLedgerAccount(String generalLedgerAccount) {

        return this.journalEntryRepository.findByJournalEntriesLedgerAccount(generalLedgerAccount);
    }

    public JournalEntry create(String transactionReference, BigDecimal amount,
                               String type, Date transactionDate) {
        Optional<JournalEntry> journalEntryOpt = this.journalEntryRepository.findByTransactionReference(transactionReference);
        if (journalEntryOpt.isPresent()) {
            throw new EntityNotFoundException("There cannot be two accounting entries with the same transaction.");
        }
        JournalEntry journalEntry = new JournalEntry();
        journalEntry.setAccountingDate(new Date());
        journalEntry.setTransactionReference(transactionReference);
        journalEntry.setTransactionDate(transactionDate);
        journalEntry.setMemo(transactionReference +" "+ type);

        JournalEntryDetail journalEntryDetail1 = new JournalEntryDetail();
        JournalEntryDetail journalEntryDetail2 = new JournalEntryDetail();

        GeneralLedgerAccount ledgerAccountAsset = ledgerAccountService.searchByType(LedgerAccountTypeEnum.ASSETS.getValue());
        GeneralLedgerAccount ledgerAccountExpenses = ledgerAccountService.searchByType(LedgerAccountTypeEnum.EXPENSES.getValue());
        GeneralLedgerAccount ledgerAccountIncome = ledgerAccountService.searchByType(LedgerAccountTypeEnum.INCOME.getValue());
        
        GeneralLedgerAccount ledgerAccountAssetDB = ledgerAccountAsset;
        GeneralLedgerAccount ledgerAccountExpensesDB = ledgerAccountExpenses;
        GeneralLedgerAccount ledgerAccountIncomeDB = ledgerAccountIncome;


        List<JournalEntryDetail> details = new ArrayList<>();
        if (type.equals(LedgerAccountTypeEnum.PAYMENTS.getValue())) {
            journalEntryDetail1.setLedgerAccount(ledgerAccountExpenses.getCode());
            journalEntryDetail1.setCredit(BigDecimal.valueOf(0));
            journalEntryDetail1.setDebit(amount);

            journalEntryDetail2.setLedgerAccount(ledgerAccountAsset.getCode());
            journalEntryDetail2.setCredit(amount);
            journalEntryDetail2.setDebit(BigDecimal.valueOf(0));

            ledgerAccountExpensesDB.setCurrentBalance(ledgerAccountAsset.getCurrentBalance().add(amount));
            ledgerAccountAssetDB.setCurrentBalance(ledgerAccountAsset.getCurrentBalance().subtract(amount));

            this.generalLedgerAccountRepository.save(ledgerAccountExpensesDB);
            this.generalLedgerAccountRepository.save(ledgerAccountAssetDB);

            details.add(journalEntryDetail1);
            details.add(journalEntryDetail2);

            journalEntry.setJournalEntries(details);
            this.journalEntryRepository.save(journalEntry);

        } else if (type.equals(LedgerAccountTypeEnum.COLLECTION.getValue())) {
            journalEntryDetail1.setLedgerAccount(ledgerAccountAsset.getCode());
            journalEntryDetail1.setCredit(BigDecimal.valueOf(0));
            journalEntryDetail1.setDebit(amount);
           
            journalEntryDetail2.setLedgerAccount(ledgerAccountIncome.getCode());
            journalEntryDetail2.setCredit(amount);
            journalEntryDetail2.setDebit(BigDecimal.valueOf(0));

            ledgerAccountAssetDB.setCurrentBalance(ledgerAccountAsset.getCurrentBalance().add(amount));
            ledgerAccountIncomeDB.setCurrentBalance(ledgerAccountAsset.getCurrentBalance().subtract(amount));

            this.generalLedgerAccountRepository.save(ledgerAccountAssetDB);
            this.generalLedgerAccountRepository.save(ledgerAccountIncomeDB);

            details.add(journalEntryDetail1);
            details.add(journalEntryDetail2);
            journalEntry.setJournalEntries(details);
            this.journalEntryRepository.save(journalEntry);
        }
        return journalEntry;
    }
}
