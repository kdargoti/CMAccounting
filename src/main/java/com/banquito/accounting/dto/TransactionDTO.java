package com.banquito.accounting.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransactionDTO {
    
    private String transactionReference;

    private BigDecimal amount;

    private String type;

    private Date transactionDate;
}
