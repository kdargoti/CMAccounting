package com.banquito.accounting.dto;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JournalEntryDetailDTO {
    
    private String ledgerAccount;

    private BigDecimal debit;

    private BigDecimal credit;
}
