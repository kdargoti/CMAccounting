package com.banquito.accounting.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JournalEntryDTO {
    
    private String groupInternalId;

    private String transactionReference;

    private Date accountingDate;

    private Date transactionDate;

    private String memo;

    private List<JournalEntryDetailDTO> journalEntries;
}
